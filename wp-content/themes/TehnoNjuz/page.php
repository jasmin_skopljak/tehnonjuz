<?php
/**
 * The template for displaying all pages.
 *
 */

get_header(); ?>
    
    <?php if (have_posts()) : while ( have_posts() ): the_post(); ?>
    
        <div class="post-single">
            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div id="page-title-wrapper">
			    <div class="page-title-border"></div>
			    <h1 class="page-title"><?php the_title();?></h1>
			</div>
			<?php if( get_theme_mod('breadcrumb') != '' ) { ?>
			    <?php echo tehnonjuz_breadcrumb(); ?>
			<?php } //endif ?>
			
			<?php if ( has_post_thumbnail() ): ?>
			<div class="page-featured-image">
			    <div class="page-image-container">
				<?php the_post_thumbnail('large_image'); ?>
			    </div>
			</div>
			<?php endif; ?>

			<div class="entry-single">
				
				<div id="content-single"><?php the_content(); ?></div>
				<?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>
				<div id="social-bar-single"><?php if ( function_exists( 'floating_social_bar' ) )  floating_social_bar( array( 'facebook' => true, 'twitter' => true, 'google' => true ) );?></div>
				<?php wp_link_pages(); ?> 
			</div> <!-- End of class #entry-single -->
			
            </div>
        </div>
    
    <?php endwhile; endif; ?>

<?php get_sidebar(); ?>
<?php comments_template(); ?>
<?php get_footer(); ?>
