<?php get_header(); ?>
		
	    <div id="main-content">         
			<div class="vijesti clearfix">		
				    <?php 
						get_template_part( 'loop', 'index' );
				    ?>
						
			</div><!-- End of .vijesti -->
			
			<div class="previous-entries"><?php previous_posts_link('&laquo; Previous Entries') ?></div>
			<div class="next-entries"><?php next_posts_link('Next Entries &raquo;','') ?></div><!-- End of pagging navigation --> 
	    
	    </div><!-- End of #main-content -->
	    
<?php get_sidebar(); ?>
<?php get_footer(); ?>