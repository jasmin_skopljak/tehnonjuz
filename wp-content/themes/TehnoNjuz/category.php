<?php

/* Tamplate for displaying categories */

get_header(); ?>

<?php if ( have_posts() ) :  ?>
    
   <div id="category-wrap">
    <div id="entry-category">
        <h1 class="entry-label"><?php printf (__('Category: %s', 'tehnonjuz'), single_cat_title('', false)); ?></h1>
    </div>
    
    <?php /* The loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>
    
        <div class="category-entry">
            <h2 class="category-post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
            <?php the_post_thumbnail('large', array( 'class' => 'aligncenter')); ?>
            <?php the_excerpt(); ?>
            
            <div class="entry-meta">
                <?php tehnonjuz_entry_meta(); ?>
                <?php edit_post_link( __( 'Edit', 'tehnonjuz' ), '<span class="edit-link">', '</span>' ); ?>
            </div><!-- .entry-meta -->
            
        </div> <!-- End of .category-entry -->
           
    <?php endwhile; ?>
    
    </div>
    
<?php else : ?>
    <?php get_template_part('content', 'none'); ?>
<?php endif; ?>
     
<?php get_sidebar(); ?>

    <div class="previous-page-category"><?php previous_posts_link('&laquo; Previous Page') ?></div>
    <div class="next-page-category"><?php next_posts_link('Next Page &raquo;','') ?></div><!-- End of pagging navigation -->  

<?php get_footer(); ?>