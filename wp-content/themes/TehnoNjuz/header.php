<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #wrap>
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />

	<title>
		<?php wp_title( '|', true, 'right' ); ?>
	</title>
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<div id="container">
	
		<header>
			<div id="header-content" class="clearfix"><!-- #logo , #searchform, #navigation, are in this section -->
				
				<?php if( get_theme_mod( 'site_logo' ) ) : ?>
					<div class="site-logo">
						<a id="logo" class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<img src="<?php echo esc_url( get_theme_mod('site_logo') ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
						</a>
					</div>
				<?php else : ?>
					<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
						<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
					</a>
				<?php endif; ?>
			
			</div><!-- End of #header-content -->
			
			<div id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array('theme_location' => 'header', 'show_home=1&include=9999', 'menu_class'=>'menu_header')); ?>
			</div>
		</header>
		
		<div class="clearfix" id="wrap">