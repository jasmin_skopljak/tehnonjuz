<aside>

	<?php
		// Check to see if widget area is active	
		if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
			<div id="widget-container">
				<div class="widget-inner">
					<div class="widget-area">
						<?php dynamic_sidebar( 'sidebar-2' ); ?>
					</div><!-- .widget-area -->
				</div><!-- .widget-inner -->
			</div><!-- #widget-container -->
	
	<?php else: ?>
	
        <!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->
	<div id="socialicons">
		<div class="social-content">
			<a href="#" title="Follow us on Twitter" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter.png" title="Follow us on Twitter" alt="Follow us on Twitter"></a>
			<a href="#" title="Like us on facebook"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/facebook.png" title="Like us on Facebook" alt="Like us on Facebook"></a>
			<a href="#" title="Connect on Google+" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/google+.png" title="Connect on Google+" alt="Connect on Google+"></a>
                </div>         
	</div>
	
	<div id="reklama1" class="clearfix">   <!-- Banner 245 x 250 in Sidebar --> 
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Banner1.png" alt="This banner can be yours">
        </div>	
	
	<?php endif; ?>
	
</aside>
