<?php
/**
 * The loop that displays posts. */
?>
	<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="entry-content">
		
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="entry">
					<h2 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
					
					<?php if (has_post_thumbnail() && ! post_password_required() ) : ?>
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('large_image') ?></a>
					<?php endif; ?>
					
					<?php the_content(); ?>
					
					<div class="entry-meta">
						<?php tehnonjuz_entry_meta(); ?>
						<?php edit_post_link( __( 'Edit', 'tehnonjuz' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-meta -->
	
				</div> <!-- End of .entry class -->
			</div>
		</div><!-- End of .entry-content -->
	<?php endwhile; ?>
	
						
	
		


