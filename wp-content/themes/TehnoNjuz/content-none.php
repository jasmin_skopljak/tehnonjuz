<?php
/**
 * The template for displaying a "No posts found" message
 *
 */
?>

<div class="posts-search">
	
	<div id="page-title-wrapper">
		<div class="page-title-border"></div>
		<h1 class="page-title"><?php _e( 'Nothing Found', 'tehnonjuz' ); ?></h1>
	</div>
	<div id="nothing-found">
	    <p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'tehnonjuz' ); ?></p>
	    <div id="content-none-form">
		<?php get_search_form(); ?>
	    </div>
        </div><!-- End of .nothing-found -->
</div><!-- End of .posts-search -->

