<?php

/* Tamplate for displaying archive pages */

get_header(); ?>

<?php if ( have_posts() ) :  ?>

    <div id="entry-category">
        <h1 class="entry-label"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'tehnonjuz' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'tehnonjuz' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'tehnonjuz' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'tehnonjuz' ), get_the_date( _x( 'Y', 'yearly archives date format', 'tehnonjuz' ) ) );
					else :
						_e( 'Archives', 'tehnonjuz' );
					endif; ?>
        </h1>
    </div>
    
<?php while ( have_posts() ) : the_post(); ?>
    
    <div class="archive-entry">
        <h2 class="archive-post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
        <div class="archive-post-content">
	    <?php the_post_thumbnail('small', array( 'class' => 'archive-post-thumbnail')); ?>
	</div>
        <?php the_excerpt(); ?>
	
	<div class="entry-meta">
	    <?php tehnonjuz_entry_meta(); ?>
	    <?php edit_post_link( __( 'Edit', 'tehnonjuz' ), '<span class="edit-link">', '</span>' ); ?>
	</div><!-- .entry-meta -->
	
    </div> <!-- End of .category-entry -->
        
<?php endwhile; ?>
    
<div class="previous-page-category"><?php previous_posts_link('&laquo; Previous Page') ?></div>
<div class="next-page-category"><?php next_posts_link('Next Page &raquo;','') ?></div><!-- End of pagging navigation -->   
         
    
<?php else : ?>
    
    <?php get_template_part('content', 'none'); ?>
    
<?php endif; ?>

<?php get_footer(); ?>