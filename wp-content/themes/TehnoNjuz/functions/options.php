<?php

/* Adds the individual sections, settings, and controls to the theme customizer
 *
 */
function tehnonjuz_customizer( $wp_customize ) {
    
    /*
    -------------------------------------------------------------------------------
    DEFINE SECTIONS IN CUSTOMIZER
    -------------------------------------------------------------------------------
    
    * General Settings Section
    */
    $wp_customize->add_section(
        'general_settings_section',
        array(
            'title' => 'General Settings',
            'description' => 'This is a general settings section.',
            'priority' => 19,
        )
    );
    
    /**
    * Header Settings Section
    */
    $wp_customize->add_section(
        'header_settings_section',
        array(
            'title' => 'Header Settings',
            'description' => 'This is a header settings section.',
            'priority' => 20,
        )
    );
    
    /**
    * Footer Settings Section
    */
    $wp_customize->add_section(
        'footer_settings_section',
        array(
            'title' => 'Footer Settings',
            'description' => 'This is a footer settings section.',
            'priority' => 21,
        )
    );
    
    $wp_customize->get_section('title_tagline')->priority = __('22', 'tehnonjuz');
    
    /*
    -------------------------------------------------------------------------------
    DEFINE SETTINGS IN CUSTOMIZER
    -------------------------------------------------------------------------------
    
    /**
    * Footer Copyright Setting
    */
    $wp_customize->add_setting(
        'copyright_textbox',
        array(
            'default' => 'Default copyright text',
            'sanitize_callback' => 'tehnonjuz_sanitize_text',
        )
    );
    
    /**
    * Breadcrumb function Setting
    */
    $wp_customize->add_setting (
        'breadcrumb',
        array(
            'sanitize_callback' => 'tehnonjuz_sanitize_checkbox',
        )
    );
    
    /**
    * Upload Logo function Setting
    */
    $wp_customize->add_setting (
        'site_logo',
        array(
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    
    /**
    * Google Fonts function Setting
    */
       
    $font_array = array('Roboto Slab','Bitter','Raleway','Khula','Open Sans','Droid Sans','Droid Serif','Roboto','Roboto Condensed','Lato','Bree Serif','Oswald','Slabo','Lora','Source Sans Pro','PT Sans','Ubuntu','Lobster','Arimo','Bitter','Noto Sans');
    $fonts = array_combine($font_array, $font_array);
      
    $wp_customize->add_setting (
        'get_fonts',
        array (
            'default' => 'Open Sans',
        )
    );
    
    /*
    -------------------------------------------------------------------------------
    DEFINE CONTROLS IN CUSTOMIZER
    -------------------------------------------------------------------------------
    /**
    * Footer Copyright Control
    */
    $wp_customize->add_control(
    'copyright_textbox',
    array(
        'label' => 'Footer copyright',
        'section' => 'footer_settings_section',
        'type' => 'text',
    )
    );
    
    /**
    * Breadcrumb Function Control
    */
    $wp_customize->add_control(
        'breadcrumb',
        array(
            'label' => 'Display Breadcrumbs',
            'section' => 'general_settings_section',
            'type' => 'checkbox',
            'priority' => '10',
        )
    );
    
    /*
    * Upload Logo Function Control
    */
    $wp_customize->add_control( new WP_Customize_Image_Control(
            $wp_customize,
            'site_logo',
            array(
                'label' => 'Upload Logo',
                'description' => 'Supported formats: .jpg,.png, .gif)',
                'section' => 'header_settings_section',
                'settings' => 'site_logo'    
            )
        )
    );
 
    /**
    * Google Fonts Function Control
    */
    
    $wp_customize->add_control(
        'get_fonts',
        array(
            'label' => 'Font Picker',
            'description' => 'Here you can choose a font for your site',
            'section' => 'general_settings_section',
            'type'  => 'select',
            'choices' => $fonts,
        )
    );
    

}
add_action( 'customize_register', 'tehnonjuz_customizer' );


/* 
 * Data Sanitization Function for TEXT
 */

function tehnonjuz_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

/* 
 * Data Sanitization Function for CHECKBOX
 */
function tehnonjuz_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

?>