<?php
    /* 
    **   Custom Modifcations in CSS depending on user settings.
    */

    function tehnonjuz_custom_css() {
        
        echo "<style id='tehnonjuz_custom_css'>";
        
        /* Post title Font*/
        if ( get_theme_mod('get_fonts') ) :
            echo "body, .vijesti, .widget-title, .widget ul { font-family: " . get_theme_mod('get_fonts') . "; }";
        endif;
        
        echo "</style>";
    }
    
    add_action('wp_head', 'tehnonjuz_custom_css');