<?php
	/* Template for displaying single posts */
?>

<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<div class="post-single">
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<h1 class="post-title"><?php the_title(); ?></h1>

			<div class="entry-single">
				
				<?php the_post_thumbnail('large_image'); ?>
				
				<div class="vijesti"><?php the_content(); ?></div>
				
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'tehnonjuz' ), 'after' => '</div>' ) ); ?>
				
				<?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>
				
				
			</div> <!-- End of class #entry-single -->
			
		</div>
		</div><!-- End of class .post-single -->
		
	<?php endwhile; endif; ?>
	
	
	<?php get_sidebar(); ?>
	
	<?php comments_template(); ?>
	
<?php get_footer(); ?>