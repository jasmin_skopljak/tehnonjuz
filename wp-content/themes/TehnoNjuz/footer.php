
</div><!--End of #wrap-->
</div> <!-- End of #container-->
	
	<footer  style="margin: 0 auto; text-align: center;">	
		<div id="footer-copyright">
			<?php if( get_theme_mod( 'copyright_textbox' ) ) : ?>
				<span><?php echo get_theme_mod ('copyright_textbox'); ?></span>
			<?php else : ?>
				<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'tehnonjuz' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'tehnonjuz' ); ?>"><?php printf( __( 'Proudly powered by %s', 'tehnonjuz' ), 'WordPress' ); ?></a>
				| Theme:<?php echo " "; bloginfo('name'); ?>
			<?php endif; ?>
		</div>
	</footer>

<?php wp_footer(); ?>
	
	<!-- Don't forget analytics -->
	
</body>

</html>
