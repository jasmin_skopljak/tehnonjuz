TehnoNjuz WordPress Theme, Copyright 2015
TehnoNjuz is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see.

===================================
Thanks for Downloading TehnoNjuz
===================================

Thank you for downloading my theme.
It is optionaly, but if you like the Theme I would appreciate it if you keep the credit link at the bottom of the page.

----------------------------------------------------------
Table of Contents	
	
	*A) Requirements
	*B) Installation
	*C) WordPress Options
	*D) Page Templates
	*E) Theme Licence
	*F) Licences of bundled resources
------------------------------------------------------------

===================================
A) Requirements
===================================
The Theme has been tested on all major web browsers, including Mozzila Firefox, Opera, Google Chrome, Internet Explorer and Safari. Theme works on the latest WordPress version without any problems, also theme supprot WordPress 3.6+.

===================================
B) Installation
===================================

	1. Automatic Installation
		Go to wp-admin > Appearance > Themes > Install Themes > Upload and choose the theme zip folder.

	2. Manual Installation
		Upload the theme files to your wordpress theme folder wp-content/themes and activate the theme in your WordPress admin panel.

To fin out more about installing WordPress theme please go http://codex.wordpress.org/Using_Themes


===================================
C) WordPress Options
===================================

You should read a few more detailed explanations about the features and recommended settings of your downloaded theme
	
	1. Post Thumbnails

	This theme use the WordPress post_thumbnail function. You can easily add your thumbnail in every post by clicking on Set Featured Image in the Featured Image box on the right, upload your picture and click Use as featured image.

	2. Media Settings

	Size of your thumbnail images is set in your WordPress media settings under Settings > Media. TehnoNjuz uses large_thumbnail size for the single post template, index page, category template, small_thumbnail for the archive template, and medium_thumbnail for the search page.

===================================
D) Page Templates
===================================

I have included "Full width Page" template for clear and simple content presentation. This template shows a static page without sidebar.

===================================
E) Theme Licence
===================================

TehnoNjuz is relased under the GNU - general public licence. That means you can use this theme on all your websites - for personal or commercial projects.

===================================
F) Licenses of bundled resources
===================================

	1. Images
	
	Images such as banner1.png in sidebar, or header.png backgound image are created by myself and licensed under GNU GPL.

	2. Fonts.

	All fonts used in the theme are from the Google API and area GPL-compatible licensed 

	3. Icons

	Social icons set called "ThemeZee Social Icons" are downloaded from http://themezee.com/free-social-media-icons/. Icons are licenced under GNU GPLv3 licence and are free to use in commercial and personal projects. 

	4. JavaScript Files

	JavaScript files licence and creators are displayed on the header of the each file
===================================
Enjoy!
===================================

Once again, thank you so much for downloading this theme.















