<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>
<div class="vijesti">
<div id="comments" class="comments-area">
	<?php if ( have_comments() ) : ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array( 'callback' => 'tehnonjuz_comment' ) );
			?>
		</ol><!-- .comment-list -->

		<?php
			// Are there comments to navigate through?
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		?>
		<nav class="navigation comment-navigation" role="navigation">
			<h1 class="screen-reader-text section-heading"><?php _e( 'Comment navigation', 'tehnonjuz' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older', 'tehnonjuz' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer &rarr;', 'tehnonjuz' ) ); ?></div>
		</nav><!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php _e( '' , 'tehnonjuz' ); ?></p>
		<?php endif; ?>
		
	<?php endif; // have_comments() ?>

	<?php comment_form(); ?>
</div><!-- #comments -->
</div> <!-- .vijesti -->