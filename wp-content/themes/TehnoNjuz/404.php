<?php
/**
 * The template for displaying a "404 error" message
 *
 */
?>

<div class="posts-search">
	
	<div id="page-title-wrapper">
		<div class="page-title-border"></div>
		<h1 class="page-title"><?php _e( '404 ERROR', 'tehnonjuz' ); ?></h1>
	</div>
	<div id="nothing-found">
	    <p><?php _e( 'Sorry, but the page you are looking for has not been found. Back Home or try searching bellow', 'tehnonjuz' ); ?></p>
	    <div id="content-none-form">
		<?php get_search_form(); ?>
	    </div>
        </div><!-- End of .nothing-found -->
</div><!-- End of .posts-search -->

