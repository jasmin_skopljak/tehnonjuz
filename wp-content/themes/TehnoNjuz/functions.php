<?php
        
    /**
    * TehnoNjuz setup.
    *
    * Sets up theme defaults and registers the various WordPress features that
    * TehnoNjuz supports.
    */
    
    
    
    
    function tehnonjuz_setup(){
        
        // Adds RSS feed links to <head> for posts and comments.
            add_theme_support( 'automatic-feed-links' );
            
        //This theme uses wp_nav_menu() in one location
        register_nav_menus( array(
            'header' => 'Header Navigation'
            ) );
        
        //This theme uses Post Thumbnails features and custom image size for featured images
        add_theme_support( 'post-thumbnails' );
	
	add_image_size ('small', 50, 50, true );
        add_image_size ('medium', 320, 155, true );
        add_image_size ('large_image', 680, 250, true );
	add_image_size ('full-width-page', 960, 240, true );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	
        /*
        * Set up the content width value based on the theme's design.
        *
        */
        global $content_width;
        if ( ! isset( $content_width ) )
            $content_width = 680;
    }
    
    add_action( 'after_setup_theme', 'tehnonjuz_setup' );

    /**
     *  Load Customizer Options
     */
    include_once('functions/options.php' );
    include_once('functions/breadcrumbs.php');
    
    /**
    * Load custom css based on user settings
    */
    include_once('functions/customizercss.php');
	
    //Excerpt of posts on category or search template page
    function tehnonjuz_excerpt_more( $more ) {
        return '';
    }
	
    add_filter('excerpt_more', 'tehnonjuz_excerpt_more');
    

    function tehnonjuz_get_font_url() {
	$font_url = '';

	/* translators: If there are characters in your language that are not supported
	 * by Open Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'tehnonjuz' ) ) {
		$subsets = 'latin,latin-ext';

		/* translators: To add an additional Open Sans character subset specific to your language,
		 * translate this to 'greek', 'cyrillic' or 'vietnamese'. Do not translate into your own language.
		 */
		$subset = _x( 'no-subset', 'Open Sans font: add new subset (greek, cyrillic, vietnamese)', 'tehnonjuz' );

		if ( 'cyrillic' == $subset )
			$subsets .= ',cyrillic,cyrillic-ext';
		elseif ( 'greek' == $subset )
			$subsets .= ',greek,greek-ext';
		elseif ( 'vietnamese' == $subset )
			$subsets .= ',vietnamese';

		$query_args = array(
			'family' => 'Open+Sans:400italic,700italic,400,700',
			'subset' => $subsets,
		);
		$font_url = add_query_arg( $query_args, "fonts.googleapis.com/css" );
	}

	return $font_url;
    }

    /**
    * Enqueue scripts and styles for front-end.
    *
    * @return void
    */

    function tehnonjuz_scripts_styles() {
	global $wp_styles;
        
        /*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
		
	// Loads our main stylesheet.
	wp_enqueue_style( 'tehnonjuz-style', get_stylesheet_uri(), array(), '2014-06-30' );
        
        $font_url = tehnonjuz_get_font_url();
	if ( ! empty( $font_url ) )
		wp_enqueue_style( 'tehnonjuz-fonts', esc_url_raw( $font_url ), array(), null );
		
	wp_enqueue_style('tehnonjuz-body-font', '//fonts.googleapis.com/css?family='.str_replace(" ", "+", get_theme_mod('get_fonts', 'Open Sans') ).':100,300,400,700' );
        
        /* Load Modernizr */     
        wp_enqueue_script( 'tehnonjuz-modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array('jquery'), '2.7.2', true );
                
    }
 
    add_action ('wp_enqueue_scripts', 'tehnonjuz_scripts_styles');
 
    /**
    * Filter the page title.
    *
    * Creates a nicely formatted and more specific title element text for output
    * in head of document, based on current view.
    *
    * @param string $title Default title text for current view.
    * @param string $sep   Optional separator.
    * @return string The filtered title.
    */
    function tehnonjuz_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'tehnonjuz' ), max( $paged, $page ) );

	return $title;
    }
    add_filter( 'wp_title', 'tehnonjuz_wp_title', 10, 2 );
    
    /**
    * Register two widget areas.
    */
    function tehnonjuz_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Main Widget Area', 'tehnonjuz' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears on posts and pages in the sidebar.', 'tehnonjuz' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'tehnonjuz_widgets_init' );

function tehnonjuz_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
			<div class="comment-author vcard">
				<?php echo get_avatar( $comment, 74 ); ?>
				<?php printf( __( '%s <span class="says">says:</span>', 'tehnonjuz' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
			</div><!-- .comment-author .vcard -->
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'tehnonjuz' ); ?></em>
				<br />
			<?php endif; ?>

			<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php
					/* translators: 1: date, 2: time */
					printf( __( '%1$s at %2$s', 'tehnonjuz' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'tehnonjuz' ), ' ' );
				?>
			</div><!-- .comment-meta .commentmetadata -->

			<div class="comment-body"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'tehnonjuz' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'tehnonjuz' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
    }
    
    if ( ! function_exists( 'twentytwelve_entry_meta' ) ) :
    /**
    * Set up post entry meta.
    *
    * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
    */
    
    function tehnonjuz_entry_meta() {
        // Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'tehnonjuz' ) );
        
        $date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);
        
        // Translators: 1 is category and 3 is the date.
	
	if ( $categories_list ) {
		$utility_text = __( 'This entry was posted in %1$s on %2$s.', 'tehnonjuz' );
	} else {
		$utility_text = __( 'This entry was posted on %2$s', 'tehnonjuz' );
	}

	printf(
		$utility_text,
		$categories_list,
		$date
	);
        
    }  
    endif;
    
    
?>