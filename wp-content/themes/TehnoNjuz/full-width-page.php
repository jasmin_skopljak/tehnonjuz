<?php
/*
Template Name: Full-width Page
*/

get_header(); ?>
    
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
        <div class="post-fullwidth">
        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
		        <div class="page-title-border"></div>
			<h1 class="page-title"><?php the_title(); ?></h1>
			
			<?php if ( has_post_thumbnail() ): ?>
			<div class="page-featured-image">
			    <div class="page-image-container">
				<?php the_post_thumbnail('full-width-page'); ?>
			    </div>
			</div>
			<?php endif; ?>
				
			<div class="entry-single">
				
				<div id="content-fullwidth"><?php the_content(); ?></div>
				<?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>
				<?php wp_link_pages(); ?> 
			</div> <!-- End of class #entry-single -->
			
		</div>
        </div>
    
    <?php endwhile; endif; ?>
  
<?php get_footer(); ?>
