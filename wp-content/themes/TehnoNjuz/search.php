<?php
/**
 * The template for displaying Search Results pages
 *
 */

get_header(); ?>

    <?php if ( have_posts() ) : ?>

	
        <div class="posts-search">
            <div class="page-header">
		
		<div id="page-title-wrapper">
		    <div class="page-title-border"></div>	
		    <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'tehnonjuz' ), get_search_query() ); ?></h1>
		</div>
	    
	    </div>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php if ( is_archive() || is_search() ) : /* Only display the excerpt of posts */ ?>
                                
                                        <div class="entry-search">
                                            <h2 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                                            <?php the_post_thumbnail('medium') ?>
                                            <?php the_excerpt(); ?>
                                            
                                            <div class="entry-meta">
                                                <?php tehnonjuz_entry_meta(); ?>
                                                <?php edit_post_link( __( 'Edit', 'tehnonjuz' ), '<span class="edit-link">', '</span>' ); ?>
                                            </div><!-- .entry-meta -->
                                        </div> <!-- .entry -->

                                <?php endif; ?>
			<?php endwhile; ?>
        </div> 
    <?php else : ?>
	<?php get_template_part( 'content', 'none' ); ?>
    <?php endif; ?>
<?php get_sidebar(); ?>

    <div class="previous-page-category"><?php previous_posts_link('&laquo; Previous Page') ?></div>
    <div class="next-page-category"><?php next_posts_link('Next Page &raquo;','') ?></div><!-- End of pagging navigation -->  

<?php get_footer(); ?>