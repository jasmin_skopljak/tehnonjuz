<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'git_tehnonjuz');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w$k06aLVn(y+%LXHtM:YeEs`&%.H4m}jkO|_QED3>mj^=rxziD1-GR_h%(~HECAh');
define('SECURE_AUTH_KEY',  'QQ)YYx*rSG#!apDMAUNQu;qRo8NMXp7gi#u,+M/D1GPS5?ra0Bo(;5NM)[~O,1a^');
define('LOGGED_IN_KEY',    '}!PXL;-<Gwq$P,[whp<z)n.McuVip(;*_gI+dgft@28(;=RS:9<]ylN6`,`L|aRE');
define('NONCE_KEY',        '3Y<~IT$hfJk8P8?R]z_bUc6^oyZz@YcHkq/)~I]r4aDV2X8Lb6||set@NNX}Xy#h');
define('AUTH_SALT',        'N4zTazV}Pvz[s:xcgn-@ym;%|w;1iJIvfko}Biw7m6xwpDLwA*=h]K?vSV~H#Y?E');
define('SECURE_AUTH_SALT', 'un1oh|XehYkM`/2q+]6><uRzOb=5SX+EUG}MTZH$/`{K6A;W7h|T8DdbdRa4Gu?=');
define('LOGGED_IN_SALT',   'iP>Fp14>?sG[/t$G)}x?E,cp3usX0Spwcs-]|QYvz$H#P97:]N!/r`W5=*cp:|(q');
define('NONCE_SALT',       'Vo!F k~>H;%]e,p}m4:k9!*JqatjiUUgduujt9|:G,LM!kV?& e%)Yv#QbG9&b?P');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
